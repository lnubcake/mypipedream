#include "stdafx.h"
#include "Canvas.h"


Canvas::Canvas(std::vector<std::vector<Tile*>*>* playgroundToSet, QWidget*)
{
	playground_ = playgroundToSet;
}


Canvas::~Canvas()
{
}

void Canvas::paintEvent(QPaintEvent * event)
{
	for (auto row : *playground_) {
		for (auto tile : *row) {
			tile->Draw(this);
		}
	}
}
