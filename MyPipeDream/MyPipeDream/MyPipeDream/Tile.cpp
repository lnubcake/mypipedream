#include "stdafx.h"
#include "Tile.h"
#include <qdebug.h>

std::optional<int> Tile::height_, Tile::width_;

Tile::Tile(unsigned x, unsigned y,unsigned maxX,unsigned maxY, const QWidget& placeholder)
{
	try {
		pipe_ = nullptr;

		// Setting the parameters of a tile
		if (!Tile::width_) { // It only needs to be initialized once
			Tile::width_ = placeholder.size().height() / maxX;
			Tile::height_ = placeholder.size().height() / maxY;
		}
		// Calculating the tile's position
		unsigned int  difference = placeholder.size().width() - placeholder.size().height();
		pos_ = new QPoint(
			difference / 2 + ((placeholder.size().height() * x / maxX) % 2 == 0 ? (placeholder.size().height() * x / maxX) : (placeholder.size().height() * x / maxX) - 1),
			((30 + placeholder.size().height() * y / maxY) % 2 == 0 ? (30 + placeholder.size().height() * y / maxY) : (30 + placeholder.size().height() * y / maxY) - 1)
		);
		qInfo() << "Tile creation successfull!";
	}
	catch (std::exception e) {
		qWarning() << "Tile creation failed";
		}
}

Tile::~Tile()
{
	delete pos_;
}

void Tile::Draw(QWidget * onWhat) const
{
	QPainter painter(onWhat);
	painter.drawRect(pos_->x(), pos_->y(), *width_, *height_);

	// Drawing the central connector
	struct Connector{
		const int width = (*width_) * 0.2;
		const int height = (*height_) * 0.2;
	} connector;
	const QPoint place = (getOrigo() + QPoint(-1 * connector.width / 2 , -1 * connector.height / 2));
	painter.fillRect(place.x(), place.y(), connector.width, connector.height, QBrush(Qt::black, Qt::SolidPattern));
	painter.fillRect(place.x() + 3, place.y() + 3, connector.width - 6, connector.height - 6, QBrush(Qt::gray,Qt::SolidPattern));
	painter.setBrush(QColor(0x700000));
	painter.drawEllipse(QPoint(getOrigo().x(), getOrigo().y()), connector.width - 17, connector.height - 17);
	painter.setBrush(QBrush(Qt::black,Qt::DiagCrossPattern));
	painter.drawEllipse(QPoint(getOrigo().x(), getOrigo().y()), connector.width - 17, connector.height - 17);
}

const QPoint Tile::getOrigo() const
{
	return QPoint(pos_->x() + *width_/2,
				pos_->y() + *height_/2);
}


