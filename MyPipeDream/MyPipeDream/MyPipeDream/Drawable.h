#pragma once
#include <QtWidgets>

class Drawable
{
public:
	virtual void Draw(QWidget* onWhat) const = 0;
	Drawable();
	virtual ~Drawable();
};

