#pragma once

#include "Pipe.h"
#include "Drawable.h"
#include <iostream>
#include <QtWidgets/QMainWindow>
#include <optional>

class Tile : public Drawable
{
	const Pipe* pipe_; // The pipe on the tile
		// TODO initialize this
		// TODO destruct this
	// In pixels
	static std::optional<int> width_;
	static std::optional<int> height_; // Width and height of the Tile
	const QPoint* pos_; // Position of left corner
public:
	Tile(unsigned x, unsigned y, unsigned maxX, unsigned maxY, const QWidget& placeholder);
	~Tile();
	void Draw(QWidget* onWhat) const final override;
private:
	const QPoint getOrigo() const;
};

