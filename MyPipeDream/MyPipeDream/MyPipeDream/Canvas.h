#pragma once
#include "Tile.h"
class Canvas : public QWidget
{
	Q_OBJECT
public:
	Canvas(std::vector<std::vector<Tile*>*>* playgroundToSet, QWidget* = 0);
	~Canvas();
protected:
	void paintEvent(QPaintEvent* event) override;
private:
	std::vector<std::vector<Tile*>*>* playground_ = nullptr;
};

