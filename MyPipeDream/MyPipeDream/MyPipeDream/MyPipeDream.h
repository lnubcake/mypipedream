#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MyPipeDream.h"
#include "Tile.h"
#include "Canvas.h"

class MyPipeDream : public QMainWindow
{
	Q_OBJECT

public:
	MyPipeDream(QWidget *parent = Q_NULLPTR);
	~MyPipeDream();

private:
	Ui::MyPipeDreamClass ui;
	Canvas* canvas;
	struct{
		unsigned int x;
		unsigned int y;
	} size;
	std::vector<std::vector<Tile*>*>* playground_;
public:
};
