#include "stdafx.h"
#include "MyPipeDream.h"
#include <exception>

MyPipeDream::MyPipeDream(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	resize(QDesktopWidget().availableGeometry(this).size() * 0.4f);

	// Size of our playground
	size.x = 3;
	size.y = 3;
	// Our playground
	try {
		playground_ = new std::vector<std::vector<Tile*>*>();
		for (unsigned y = 0; y < size.y; y++) {
			playground_->push_back(new std::vector<Tile*>());
			for (unsigned x = 0; x < size.x; x++) {
				playground_->at(y)->push_back(new Tile(x, y,size.x,size.y, *(this)));
				
			}
		}
		qInfo() << "Playground creation successfull!";
	}
	catch (std::exception e) {
		qWarning() << "Playground creation failed!";
	}
	
	// Setting up our central widget
	// This is where all drawings will happen
	canvas = new Canvas(playground_);
	setCentralWidget(canvas);
	ui.centralWidget->resize(QDesktopWidget().availableGeometry(this).size()*0.5);
	resize(QDesktopWidget().availableGeometry(this).size() * 0.5f);
}

MyPipeDream::~MyPipeDream()
{
	for (auto row : *playground_) {
		for (auto tile : *row) {
			delete tile;
		}
		delete row;
	}
	delete playground_;
}
